package app;

import java.io.IOException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParseException;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void givenFieldIsIgnoredByName_whenDtoIsSerialized_thenCorrect()
        throws JsonParseException, IOException {

        String jsonInString = "{\"id\":1,\"content\":\"my comment\", \"approved\": \"no\"}";
        ObjectMapper mapper = new ObjectMapper();
		Comment myComment = mapper.readValue(jsonInString, Comment.class);

        String commentAsString = mapper.writeValueAsString(myComment);

        assertThat(commentAsString, not(containsString("id")));
        assertThat(commentAsString, not(containsString("approved")));
    }
}
