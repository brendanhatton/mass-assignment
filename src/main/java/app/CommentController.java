package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class CommentController {

    /**
     * A sudo storage to save comments
     */
    private ArrayList<Comment> storage = new ArrayList<Comment>();

    /**
     * Gets JSON from POST request body of users and
     * stores.
     * @return comment content
     */
    @PostMapping("/submit")
    public String newComment(@RequestBody Comment comment) {
        this.storage.add(comment);
        return "Your comment '" + comment + "' has been successfully stored";
    }
}
